package Steps;

import java.lang.reflect.Executable;

import org.junit.AfterClass;

import Drivers.Driver;
import Elementos.Elementos;
import Metodos.Metodos;
import Run.Executar;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps extends Driver {
	
	Metodos metodos = new Metodos();

	Elementos el = new Elementos();
	
	
	@Given("que eu acesse o site com dados validos")
	public void queEuAcesseOSiteComDadosValidos() {
		Executar.abrirNavegador();
		metodos.escrever(el.loginUser, "standard_user");
		
	
	}
	@Given("clicar em dois itens para adicionar ao carrinho")
	public void clicarEmDoisItensParaAdicionarAoCarrinho() {
	    
	}
	@When("clico no carrinho de compras")
	public void clicoNoCarrinhoDeCompras() {
	   
	}

	@Then("concluo a compra")
	public void concluoACompra() {
	    
		
	}


	@Then("valido a mensagem de sucesso")
	public void validoAMensagemDeSucesso() {
	    
	}
	
	@AfterClass
	public static void finalizarTest() {
		driver.quit();
		System.out.println("***************TESTE FINALIZADO COM SUCESSO!!!***************");

	}
	
	




}
